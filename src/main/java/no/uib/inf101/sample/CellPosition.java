package no.uib.inf101.sample;

/** A CellPosition contains a row and a column. */
public record CellPosition(int row, int col) {

}
